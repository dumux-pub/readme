# How to generate a DuMux Pub module

## Generation of the module

* If you want to extract a subset of an existing module, run the script [extract_as_new_module.py](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/bin/extract_as_new_module.py).

* Create an install script for your module by running [make_installscript.py](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/bin/make_installscript.py).

* Ask an [owner](https://git.iws.uni-stuttgart.de/groups/dumux-pub/-/group_members) of the `dumux-pub` group to generate a new repository.

* Follow the instructions on the GitLab page of the repository to push your local folder.

* Grant public read access by adapting the module's "Settings" &rarr; "General" &rarr; "Visibility, project features, permissions". If you don't have sufficient rights to do this, ask one of the group [owners](https://git.iws.uni-stuttgart.de/groups/dumux-pub/-/group_members).

* Add a file `LICENSE.md` by, for example, copying and adapting [LICENSE_template.md](LICENSE_template.md).

## Docker container

### Generate the necessary files

From the top level of the Pub module, run the script [create_dockerimage.py](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/bin/create_dockerimage.py):
```sh
python3 path/to/most/recent/dumux/bin/create_dockerimage.py -m MODULEPATH -i INSTALLSCRIPT [-t TEMPLATEFOLDER]
```
It is recommended to use the latest version of the script which is probably contained in a folder that is different from the one used by the Pub module. In this case, it is necessary to use also the `-t` option with the value `path/to/most/recent/dumux/docker`.

The script will generate a folder `docker` which contains all necessary files to build the Docker image, particularly the `Dockerfile`. The script asks if the imgae should be built directly. It is recommended to first check and eventually customize the auto-generated files.

### Customize and build

* Check the `Dockerfile` if the base image and the installed packages look ok.

* Adapt the `WELCOME` message to guide the user to a good entry point.

* From the `docker` folder, build the image as suggested by `create_dockerimage.py`:
```sh
docker build -f ./Dockerfile -t MODULENAME .
```

* If necessary, iterate until successful.

* Follow the instructions from `docker/README.md`. In particular, spin up a container by
```sh
./docker_MODULENAME.sh open
```

### Register and finalize the repository

* Follow the instructions on the module's registry page `.../dumux-pub/REPONAME/container_registry` to push the image to the registry.

* Replace the image name in `docker_MODULENAME.sh` with the actual image name `git.iws.uni-stuttgart.de:4567/dumux-pub/REPONAME`.

* Test by following the user instructions at the end of `docker/README.md`. To be on the safe side, first delete your local image by `docker image rm ...` and any possibly active container by `docker rm ...`.

* Adapt the module's `README.md` and commit the `docker` folder.
